---
title: Wikifolio Update KW34
date: 2017-08-28
draft: true
---

# Trendfolge Deutschland

| Aktie | WKN | Buy | Letztes Tief | CRV | Kauf |
| --- | :---: | ---: |---: |---: | ---
| Covestro AG O.N. | 606214 | 68,56 | 65,00 | 12,40 | x |
| Hella KGAA Hueck | A13SX2 | 46,43 | 44,91 | 11,96 | x |
| Stada Arzneimittel | 725180 | 83,20 | 63,20 | 23,08 | x |



[Trendfolge Deutschland Wikifolio](https://www.wikifolio.com/de/de/w/wf0fherr02)

# Trendfolge Small Caps

[Trendfolge Small Caps](https://www.wikifolio.com/de/de/w/wf0fherr04)
